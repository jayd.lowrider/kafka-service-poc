package com.zase.kafka.consumer.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import com.zase.kafka.consumer.KConsumer;
import com.zase.kafka.listener.ConsumerListener;

public class KConsumerImpl implements KConsumer, Runnable {

	private final ConsumerConnector consumer;
	private final String topic;
	private final ConsumerListener listener;

	public KConsumerImpl(String topic, ConsumerConfig config, ConsumerListener ilistener) {
		consumer = kafka.consumer.Consumer
				.createJavaConsumerConnector(config);
		this.topic = topic;
		this.listener = ilistener;
	}
	
	public void run() {
		
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
	    topicCountMap.put(topic, new Integer(1));
	    Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
	    KafkaStream<byte[], byte[]> stream =  consumerMap.get(topic).get(0);
	    ConsumerIterator<byte[], byte[]> it = stream.iterator();
	    assembleData(it);
	}
	
	
	private void assembleData( ConsumerIterator<byte[], byte[]> it ){
		
		while(it.hasNext()){
			String data = new String(it.next().message());
			//System.out.println("CONSUMER: " + data);
			listener.callbackConsumerData(data);
		}
	}


	
		
}
