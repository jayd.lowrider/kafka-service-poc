package com.zase.kafka.listener;

public interface ConsumerListener {
	
	void callbackConsumerData(String data);
}
