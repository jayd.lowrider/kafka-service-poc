package com.zase.kafka.producer;

import java.util.List;

public interface KProducer {
	
	void sendMessageOne(String msg,  String topic);
	void sendMessageBulk(List<String> list, String topic);
		
}
