package com.zase.kafka.producer.impl;

import java.util.List;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import com.zase.kafka.producer.KProducer;


public class KProducerImpl implements KProducer {

	
	private Producer<String, String> producer = null;
	
	public KProducerImpl(ProducerConfig config){
		producer = new Producer<String, String>(config);
	}

	
	public void sendMessageOne(String msg,  String topic){
		KeyedMessage<String, String> data = new KeyedMessage<String, String>(topic, msg);
		producer.send(data);
	}
	
	public void sendMessageBulk(List<String> list, String topic){
		
		for (String l : list)
			producer.send(new KeyedMessage<String, String>(topic, l));
	}
	
}
