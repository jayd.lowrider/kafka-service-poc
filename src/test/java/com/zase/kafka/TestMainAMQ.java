package com.zase.kafka;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.commons.io.FileUtils;

public class TestMainAMQ extends BaseTestMain{
	
	
	private static Connection cn;
	private static Session sess; 
	private static MessageProducer producer;
	private static MessageConsumer consumer;
	
	public static void main(String[] args){
		
		if (args.length == 0 || args.length == 1){
			System.err.println("Args was not provided. 1 arg is the size and 2 arg is you want to put sleep or not. ");
			System.exit(-1);
		}
		
		// first arg = size
		// second arg = withsleep boolean
		
		int size = Integer.valueOf(args[0]);
		boolean withSleep = Boolean.valueOf(args[1]);
	
		
		TestMainAMQ tM = new TestMainAMQ();
		long start = System.currentTimeMillis();
		long end = 0;
		tM.generalConfigRun(size, withSleep);
		try {
			countDown.await();
			
			//close all.
			consumer.close();
			producer.close();
			sess.close();
			cn.close();
			
			service.shutdownNow();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			end = System.currentTimeMillis();
		}
		
		System.out.println("  ");
		System.out.println("====================================");
		System.out.println("Time took to run a producer-consumer AMQ with messages="+ size 
				+ " withSleepOn=" + withSleep + " is=" + (end-start)/1000 + " in secs.");
		System.out.println("====================================");
		System.out.println("  ");
		
		System.exit(0);
	}
	
	public TestMainAMQ(){
		super();
	}

	public void generalConfigRun(final int size, final boolean withSleep) {
		
		try{
			BrokerService broker = new BrokerService();
			File tmpFile = new File("/tmp/kahaDB");
			FileUtils.deleteDirectory(tmpFile);
			tmpFile.mkdir();
			broker.setDataDirectoryFile(tmpFile);
			broker.addConnector("nio://localhost:51518");
			broker.start();
			
			//Regular Connector
			//ConnectionFactory factory = new ActiveMQConnectionFactory("nio://localhost:51518");
			
			//Optimized Connector
			ConnectionFactory factory = new ActiveMQConnectionFactory("failover:(nio://localhost:51518)?randomize=true&initialReconnectDelay=10&maxReconnectAttempts=-1&backup=true&jms.watchTopicAdvisories=false&jms.useCompression=true&jms.prefetchPolicy.all=1000&jms.optimizeAcknowledge=true&jms.useAsyncSend=true");
			cn = factory.createConnection();
			cn.start();
			
			sess = cn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic topic = sess.createTopic("test-topic");
			
			//create a producer on topic
			producer = sess.createProducer(topic);
			setupAndRunProducer(size, withSleep, producer, sess);
			
			final AtomicInteger counter = new AtomicInteger(size);
			consumer = sess.createConsumer(topic);
			consumer.setMessageListener(new MessageListener() {
				
				public void onMessage(Message message) {
					
					if (message instanceof TextMessage){
						try {
							int cur = counter.decrementAndGet();
							String data=((TextMessage) message).getText();
							consumerCountPrintOut(cur, data);
							
							if (cur <=5){
								System.out.println("Consumer Countdown to count="+ cur);
							}
							
							if (cur <= 1){
								System.out.println("Current consumer count:" +cur + " size=" + data.length());
								countDown.countDown();
							}
						} catch (JMSException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
				}
			});
		
		}
		catch (JMSException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	} 
	
	
	private void setupAndRunProducer(final int size, 
									final boolean withSleep, final MessageProducer producer,
									final Session sess){
		service.execute(new Runnable() {
			
			public void run() {
				
				int sizeTemp = size;
				while ( sizeTemp-- > 0  ){
					
					producerCountPrintOut(sizeTemp);
					
					TextMessage m;
					try {
						m = sess.createTextMessage();
						m.setText(TestMainKafka.TEST_MESSAGE);
						producer.send(m);
						withSleepOn(withSleep);
						
					} catch (JMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				countDown.countDown();
			}
		});
	}
}
