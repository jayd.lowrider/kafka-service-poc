package com.zase.kafka;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

//import storm.kafka.KafkaConfig.StaticHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.Scheme;
//import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.TupleImpl;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import com.google.common.collect.ImmutableList;

public class TestMainSpoutAndBolts extends TestMainKafka implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6398478358076085591L;

	public static void main(String[] args) {

		if (args.length == 0 || args.length == 1) {
			System.err
					.println("Args was not provided. 1 arg is the size and 2 arg is you want to put sleep or not. ");
			System.exit(-1);
		}

		// first arg = size
		// second arg = withsleep boolean

		int size = Integer.valueOf(args[0]);
		boolean withSleep = Boolean.valueOf(args[1]);

		TestMainSpoutAndBolts tM = new TestMainSpoutAndBolts();
		long start = System.currentTimeMillis();
		long end = 0;
		tM.generalConfigRun(size, withSleep);
		try {
			countDown.await();
			service.shutdownNow();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			end = System.currentTimeMillis();
		}

		printOutputAndExit("spout-bolts", withSleep, end, start, size);
	}

	@Override
	public void generalConfigRun(int sampleSet, boolean withSleep) {
		String topic = "topic-spout";

		// this is your producer
		startAndPublishMessage(super.configsetup(), topic, sampleSet,
				withSleep, "WORK, WORK, WORK, TESR, WORM, WORRK, WORK");

		// this is your consumer.
		this.createSimpleSpoutandBolt();
	}

	public enum AckStrategy {
		ACK_IGNORE, ACK_ON_RECEIVE, ACK_ON_WRITE;
	}

	public static class CountingWordBolt implements IRichBolt {

		protected AckStrategy ackStrategy = AckStrategy.ACK_IGNORE;
		protected OutputCollector collector;
		protected TopologyContext context;
		protected Map prepareParamMap;
		private Map<String, Integer> wordMap = new HashMap<String, Integer>();

		public CountingWordBolt() {

		}

		public void prepare(Map paramMap, TopologyContext paramTopologyContext,
				OutputCollector paramOutputCollector) {

			prepareParamMap = paramMap;
			context = paramTopologyContext;
			collector = paramOutputCollector;
		}

		public void execute(Tuple paramTuple) {
			if (this.ackStrategy == AckStrategy.ACK_ON_RECEIVE) {
				this.collector.ack(paramTuple);
			}
			// do the work.
			String current = (String) paramTuple.getValues().get(0);
			int i = 0;
			if (wordMap.containsKey(current)) {
				i = wordMap.get(current).intValue();
			}

			wordMap.put(current, Integer.valueOf(i));
			collector.emit(Utils.tuple(new Object[] { current,
					Integer.valueOf(i) }));
		}

		public void cleanup() {
			// nothing to cleanup yet.
		}

		public void declareOutputFields(
				OutputFieldsDeclarer paramOutputFieldsDeclarer) {
			// TODO Auto-generated method stub
			System.out.println("declarerOutputFields methods called.");
			System.out.println(" OutputFieldsDeclarer "
					+ paramOutputFieldsDeclarer.toString());
		}

		public Map<String, Object> getComponentConfiguration() {
			// TODO Auto-generated method stub
			return null;
		}

		public void setAckStrategy(AckStrategy ack) {
			this.ackStrategy = ack;
		}
	}
	
	
	private void createSimpleSpoutandBolt() {
		
		TopologyBuilder builder = new TopologyBuilder();
        List<String> hosts = new ArrayList<String>();
        hosts.add("localhost");
        
		SpoutConfig spoutConf = null; //ew SpoutConfig(
				//hosts, 1, "topic-spout", "localhost", "id"); 
        
       // SpoutConfig spoutConf = SpoutConfig.fromHostStrings(hosts, 8, "clicks", "/kafkastorm", "id");
        //spoutConf.scheme = new SchemeAsMultiScheme(new StringScheme());
        spoutConf.forceStartOffsetTime(-2);

        spoutConf.zkServers = new ArrayList<String>() {{
           add("localhost");
        }};
        spoutConf.zkPort = 2181;
        
        CountingWordBolt bolt = new CountingWordBolt();
        builder.setSpout("spout", new KafkaSpout(spoutConf), 3);
        builder.setBolt("loader", bolt).shuffleGrouping("spout"); 
        Config conf = new Config();
        conf.setDebug(true);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("kafka-test", conf, builder.createTopology());
		
	}
	
	private void createSimpleSpoutandBolt1() {

		// this is basically a spout
		TopologyBuilder builder = new TopologyBuilder();
		List<String> hosts = new ArrayList<String>();
		hosts.add("localhost");

		SpoutConfig spoutConfig = null; //ew SpoutConfig(
				//hosts, 1, "topic-spout", "localhost", "id"); // fromHostStrings(hosts,
																// 1, "test",
																// "/foo",
																// "foo");
		spoutConfig.zkServers = ImmutableList.of("localhost");
		spoutConfig.zkPort = 2181;
		//spoutConfig.scheme = new SchemeAsMultiScheme( new StringScheme());
		builder.setSpout("spout", new KafkaSpout(spoutConfig));
		
		CountingWordBolt bolt = new CountingWordBolt();
		bolt.setAckStrategy(AckStrategy.ACK_ON_WRITE);
		builder.setBolt("loader", bolt).shuffleGrouping("spout"); // shuffling
																	// for now.
		StormTopology top =  builder.createTopology();
		
		Fields f = new Fields("VALUE");
		Values v = new Values("WORK");
//		TopologyContext context = new 
//				TopologyContext(top, new HashMap<String, String>(), new HashMap<Integer, String>(), null, null, null, null, null,
//		                -1, -1, new ArrayList<Integer>(), null, null, null, null, null);
//		
//		bolt.prepare(null, context, null);
//		
//		Tuple t = new TupleImpl(context, v, 0, "WORK");
//
//		bolt.execute(t);
	}

}
