package com.zase.kafka;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class BaseTestMain {
	
	protected static ScheduledExecutorService service = Executors.newScheduledThreadPool(100);
	protected static CountDownLatch countDown = new CountDownLatch(2);
	
	protected BaseTestMain() {
	}
	
	
	public abstract void generalConfigRun( int sampleSet, boolean withSleep);
	
	protected void consumerCountPrintOut( int cur, String data ){
		
		if (cur < 0) return;
		
		if ((cur % 5) == 0){
			System.out.println("Current Consumer count=" + cur + " data=" + data);
		}
	}
	
	
	protected void producerCountPrintOut( int cur){
		if (cur % 5 == 0){
			System.out.println("Current Producer count=" + cur);
		}
	}
	
	protected void withSleepOn(boolean withSleep){
		try {
			if ( withSleep ){
				System.out.println(Thread.currentThread().getName() + " Sleeping in 5 ... ");
				TimeUnit.SECONDS.sleep(5);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}