package com.zase.kafka;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import kafka.consumer.ConsumerConfig;
import kafka.producer.ProducerConfig;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;

import org.apache.commons.io.FileUtils;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig.ConfigException;

import storm.kafka.BrokerHosts;
import storm.kafka.ZkHosts;

import com.zase.kafka.consumer.impl.KConsumerImpl;
import com.zase.kafka.listener.ConsumerListener;
import com.zase.kafka.producer.KProducer;
import com.zase.kafka.producer.impl.KProducerImpl;

public class TestMainKafka extends BaseTestMain {
	
	public static final String TEST_MESSAGE="Well, I’m usually hacking Scala in Intellij, so why not using it as a ‘decompiler’. When you drop a class file into IntelliJ it shows the signature of the class. Well, with the Scala plugin installed it shows a Scala outline, which is nice. However when you want to see the raw signature it is kind of pointless. Without the Scala plugin you will see the regular Java outline";	
	
	private KafkaServerStartable kafkaServer;
	
	public TestMainKafka(){
		super();
	}
	
	
	public static void main(String[] args) throws IOException, ConfigException{

		
		if (args.length == 0 || args.length == 1){
			System.err.println("Args was not provided. 1 arg is the size and 2 arg is you want to put sleep or not. ");
			System.exit(-1);
		}
		
		// first arg = size
		// second arg = withsleep boolean
		
		int size = Integer.valueOf(args[0]);
		boolean withSleep = Boolean.valueOf(args[1]);
	
		
		TestMainKafka tM = new TestMainKafka();
		tM.startServer();
		long start = System.currentTimeMillis();
		long end = 0;
		tM.generalConfigRun(size, withSleep);
		try {
			countDown.await();
			service.shutdownNow();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			end = System.currentTimeMillis();
		}
		
		printOutputAndExit("producer-consumer",withSleep, end, start, size);
		
	} 
	
	protected static void printOutputAndExit(String id,  boolean withSleep, long end, long start, int size)
	{

		System.out.println("  ");
		System.out.println("====================================");
		System.out.println("Time took to run a "+ id + " Kafka with messages="+ size 
				+ " withSleepOn=" + withSleep + " is=" + (end-start)/1000 + " in secs.");
		System.out.println("====================================");
		System.out.println("  ");
		
		System.exit(0);
	}	
	
	
	
	protected KProducer configsetup(){
		Properties props = new Properties();
		props.put("zk.connect", "127.0.0.1:2000");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("metadata.broker.list", "localhost:9092");
		ProducerConfig config = new ProducerConfig(props);
		
		
		// start the 
		return new KProducerImpl(config);
	}
	
	private void startServer() throws IOException, ConfigException{
		
		//==Start of zookeeper...
		Properties p = new Properties();
		
		File zF = new File("/tmp/zookeeper");
		FileUtils.deleteDirectory(zF);
		zF.mkdir();
		
		
		p.put("dataDir", zF.toPath().toFile().toString());
		p.put("clientPort", "2000");
		p.put("maxClientCnxns", "0");
		
		QuorumPeerConfig peer = new QuorumPeerConfig();
		peer.parseProperties(p);

		final ZooKeeperServerMain zooKeeperServer = new ZooKeeperServerMain();
		final ServerConfig configuration = new ServerConfig();
		configuration.readFrom(peer);
		
		new Thread() {
		    public void run() {
		        try {
		            zooKeeperServer.runFromConfig(configuration);
		        } catch (IOException e) {
		        	System.err.println("ZooKeeper Failed" + e );
		        }
		    }
		}.start();

		//==End
		
		File tmpDir = new File("/tmp/kafka-logs");
		FileUtils.deleteDirectory(tmpDir);
	    tmpDir.mkdir();
	    
        Properties props = new Properties();
        props.put("port", 9092 + "");
        props.put("broker.id", 1 + "");
        props.put("log.dir",tmpDir.toPath().toFile().toString());
        props.put("num.network.threads", 2 + "");
        props.put("socket.send.buffer.bytes",1048576 + "");
        props.put("socket.request.max.bytes",1048576 + "" );
        props.put("num.partitions",1 + "");
        props.put("log.flush.interval.messages",10000 + "");
        props.put("log.flush.interval.ms",1000 + "" );
        props.put("log.retention.hours",168 + "" );
        props.put("log.retention.bytes",1073741824 + "" );
        props.put("log.segment.bytes",536870912 + "" );
        props.put("log.cleanup.interval.mins",1 + "" );
        props.put("zookeeper.connection.timeout.ms",1000000 + "" );        		
        
        props.put("zookeeper.connect", "localhost:2000");
		
		KafkaConfig kafkaConfig = new KafkaConfig(props);

        kafkaServer = new KafkaServerStartable(kafkaConfig);
        kafkaServer.startup();
        
        BrokerHosts brokerHosts = new ZkHosts("localhost:2000");
        //don't worry about killing kafka as we are finish as it will 
	}
	
	
	public void generalConfigRun( int sampleSet, boolean withSleep){
		String topic = "test-topic";
	
		startAndPublishMessage(configsetup(), topic, sampleSet, withSleep, TEST_MESSAGE);
		
		final AtomicInteger counter = new AtomicInteger(sampleSet);
		service.execute(new KConsumerImpl(topic, createConsumerConfig(), new ConsumerListener() {
			
			public void callbackConsumerData(String data) {
				// do nothing with the data.
				int cur = counter.decrementAndGet(); 
				consumerCountPrintOut(cur, data);
				if (cur == 0){
					countDown.countDown();
				}
			}
		}));
	}
	
	
	public void startAndPublishMessage(final KProducer producer, final String topic, 
									   final int numberOfData, final boolean withSleep,
									   final String data){
		
		service.execute(new Runnable() {
			
			public void run() {
				int sampleSet = numberOfData;
				while (sampleSet-- != 0 ){
					
					producerCountPrintOut(sampleSet);
					
					producer.sendMessageOne(data
											, topic);
					withSleepOn(withSleep);
				}
				
				countDown.countDown();
				
			}
		});
	}
	
	private static ConsumerConfig createConsumerConfig() {
		Properties props = new Properties();
		props.put("zookeeper.connect", "localhost:2000");
		props.put("group.id", "test-consumer-group");
		props.put("zookeeper.session.timeout.ms", "400");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000");

		return new ConsumerConfig(props);

	}
}
